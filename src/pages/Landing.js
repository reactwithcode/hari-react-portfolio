import React, {useContext} from 'react';
import profilePic from '../images/hari-pic.jpg';
import {Link} from 'react-router-dom';
import { Row, Col, Image } from 'react-bootstrap';
import Footer from '../components/Footer';
import '../App.css';
import Navbar from "../components/Navbar";
import htmlIcon from '../images/html-icon.png'
import cssIcon from '../images/css-icon.png'
import sassIcon from '../images/sass-icon.png'
import reactIcon from '../images/react-icon.png'
import reduxIcon from '../images/redux-icon.png'
import nodejsIcon from '../images/nodejs-icon.jpeg'
import techStopImg from '../images/techstop.png'
import meowMeImg from '../images/meowmetv.png';

function Landing() {

  return (
    <>
<Navbar />
<div className="container justify-content-center">
<section id="hero">
  <div className="row container justify-content-center">
    <div className="col-lg-6 col-md-6 col-sm-6">
      <h1>Hi<br/>I'm Hari</h1>
      <h1 className="fe-title">I'm passionated frontend developer.</h1>
      <p id="fe-desc">I love everything about code, I solve problems. I'm from Sidoarjo city of Indonesia. Feel free to contact me 😊</p>
      <a className="btn btn-primary btn-large btn-shadow contact-btn" href="mailto:thohari.akbar@gmail.com" role="button">Contact Me</a>
    
      <div className="sosmed-container">
        <a href="https://github.com/thohariakb" target="_blank" rel="noreferrer"><i class="icon fa fa-github-alt fa-2x"></i></a>
        <a href="http://gitlab.com/thohariakb" target="_blank" rel="noreferrer"><i class="icon fa fa-gitlab fa-2x"></i></a>
        {/* <a href="https://behance.net/thohariakb" target="_blank" rel="noreferrer"><i class="icon fa fa-behance fa-2x"></i></a> */}
        <a href="https://www.linkedin.com/in/muchammad-thohari-akbar-52540216a/" target="_blank" rel="noreferrer"><i class="icon fa fa-linkedin fa-2x"></i></a>
      </div>

    </div>

    <div className="col-lg-6 col-md-6 col-sm-12 d-flex justify-content-center">
      <img src={profilePic} style={{width: '40vw'}} className="img-profile align-self-center" alt="hari-pic" />
    </div>

</div>

</section>


<div className="item content-containers"> 

<section id="portfolios">
    <div className="container justify-content-center">
      <div className="row ms-5 mt-5 mb-3 d-flex justify-content-center">
        <div className="col">
          <h1>Portfolios</h1>
          <p>Here are my other portfolios.</p>
        </div>
      </div>
    
      <div className="row ms-5 mt-5 mb-3 d-flex justify-content-center cards">
  
        {/* <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/Clima-App" target="_blank" rel="noreferrer" alt="Service One"></a>
              <h5 className="card-title">Clima-App</h5>
              <p className="card-text">Clima App - A awesome weather android app that shows you the temperature accurately on your current location and you can choose location manually also.</p>
            </div>
          </div>
        </div>
  
        
        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/AyoKerja" target="_blank" rel="noreferrer" alt="Service One"></a>
              <h5 className="card-title">AyoKerja</h5>
              <p className="card-text">Ayo Kerja is a talent agency platform for helping recruiters with various professional talents.</p>
            </div>
          </div>
        </div>
  
        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/FlashChat-Firebase-App" target="_blank"  alt="Service One"></a>
              <h5 className="card-title">FlashChat Firebase Android App</h5>
              <p className="card-text">An android messenger app that allows you to make a private chat with your friends.</p>
            </div>
          </div>
        </div> */}
  
        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <a href="http://tech-stop.herokuapp.com/" target="_blank">
            <img src={techStopImg} className="card-img-top w-100" alt="techstop-image"/>
            </a>
            <div className="card-body">
              <a className="icon fa fa-gitlab fa-2x" href="https://gitlab.com/binarxglints_batch11/finalproject/team-d/front-end" target="_blank"  alt="Techstop"></a>
              <h5 className="card-title">Tech Stop</h5>
                <p className="card-text text-start">Tech Stop is a marketplace based on web application that is providing services for your damaged stuff, it is an one-stop marketplace for all your technical service needs!
                </p>
                <p className="card-text text-start">
                  Customers can conveniently hire the best service for the best price,
                  customers can conveniently hire the best service for the best price.
                </p>
            </div>
          </div>
        </div>


        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <a href="http://meowmetv.herokuapp.com/" target="_blank">
              <img src={meowMeImg} className="card-img-top w-100" alt="meowme-image"/>
            </a>
            <div className="card-body">
              <a className="icon fa fa-gitlab fa-2x" href="https://gitlab.com/thohariakb/meowmetv/" target="_blank"  alt="Techstop"></a>
              <h5 className="card-title">Meowme</h5>
                <p className="card-text text-start">
                Meowme offers movie information that has released and the upcoming movie releases, If you are looking for what ordinary viewing folk think of this week’s releases, there is really nowhere else than Meowme. 
                </p> 
                <p className="card-text text-start">
                There are countless user reviews of each film title, information of cast actors, ranging from a 1-star review up to a 5!
                </p>
            </div>
          </div>
        </div>

        {/* <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/AyoKerja" target="_blank" rel="noreferrer" alt="Service One"></a>
              <h5 className="card-title">AyoKerja</h5>
              <p className="card-text">Ayo Kerja is a talent agency platform for helping recruiters with various professional talents.</p>
            </div>
          </div>
        </div>
   */}
        
  
      </div>

    </div>
    </section>

  </div>

<div className="mt-5 mb-3 d-flex justify-content-center">
  <Row><h1>Skills</h1></Row>
</div>
<div className="item content-containers mb-5 p-5 d-flex justify-content-center"> 
<Row>
  <Col><Image src={htmlIcon} style={{width: '100px', marginTop: '20px'}}></Image></Col>
  <Col><Image src={cssIcon} style={{width: '100px', marginTop: '20px'}}></Image></Col>
  <Col><Image src={sassIcon} style={{width: '100px', marginTop: '20px'}}></Image></Col>
  <Col><Image src={reactIcon} style={{width: '100px', marginTop: '20px'}}></Image></Col>
  <Col><Image src={reduxIcon} style={{width: '100px', marginTop: '20px'}}></Image></Col>
  <Col><Image src={nodejsIcon} style={{width: '100px', marginTop: '20px'}}></Image></Col>
</Row>
</div>

 </div>
 <Footer />
    </>
  );
}

export default Landing;
